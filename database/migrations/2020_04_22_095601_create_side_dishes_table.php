<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSideDishesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('side_dishes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('manu_name')->nullable();
			$table->text('dish_name')->nullable();
			$table->integer('dish_id')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('side_dishes');
	}

}
