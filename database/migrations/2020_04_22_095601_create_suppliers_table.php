<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSuppliersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('suppliers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('supplier_name')->nullable();
			$table->text('supplier_address')->nullable();
			$table->text('supplier_contact')->nullable();
			$table->integer('price_histories_id')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->text('product')->nullable();
			$table->text('current_price')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('suppliers');
	}

}
