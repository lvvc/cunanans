<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIngredientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ingredients', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('item_name')->nullable();
			$table->text('weight')->nullable();
			$table->text('unit_of_measurement')->nullable();
			$table->text('price_history')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->text('supplier')->nullable();
			$table->text('current_price')->nullable();
			$table->text('description')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ingredients');
	}

}
