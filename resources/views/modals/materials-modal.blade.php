<!-- Modal -->
<div class="modal fade" id="materials_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Create Materials</h5>
			</div>
			<div class="modal-body">

				<form id="form" action="/admin/materials" method="POST" enctype="multipart/form-data">
				
					@csrf

					<div class="modal-card em20 mb-2">


						<div class="form-group  col-md-12 modal-input">
		
							<label class="control-label" for="name">Material Name</label>
							<input type="text" class="form-control" name="material_name">
							
						</div>

						<div class="form-group  col-md-6 modal-input">
		
							<label class="control-label" for="name">Unit Cost</label>
							<input type="text" class="form-control" name="unit_cost">
							
						</div>

						<div class="form-group  col-md-6 modal-input">
		
							<label class="control-label" for="name">Unit Price</label>
							<input type="text" class="form-control" name="unit_price">
							
						</div>

						<div class="form-group  col-md-12 modal-input">
		
							<label class="control-label" for="name">Remarks</label>
							<textarea class="form-control" name="remarks" rows="3"></textarea>
							
						</div>



					</div>

				</form>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-red" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-blue" onclick="submit_form()">Done</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	
	function submit_form(){
		$('#form').submit();
	}

</script>