<!-- Modal -->
<div class="modal fade" id="ingredients_edit_modal{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Edit Ingredient</h5>
			</div>
			<div class="modal-body">

				<form id="form_edit{{$data->id}}" action="/admin/ingredients/{{$data->id}}" method="POST" enctype="multipart/form-data">
				
					@csrf

					<input type="hidden" name="_method" value="PUT">

					<div class="modal-card em11 mb-2">


						<div class="form-group  col-md-6 modal-input">
		
							<label class="control-label" for="name">Item Name</label>
							<input type="text" class="form-control" name="item_name" value="{{$data->item_name}}">
							
						</div>

						<div class="form-group  col-md-6 modal-input">
		
							<label class="control-label" for="name">Description</label>
							<input type="text" class="form-control" name="description" value="{{$data->description}}">
							
						</div>

						<div class="form-group  col-md-6 modal-input">
		
							<label class="control-label" for="name">Unit Of Measurement</label>
							<input type="text" class="form-control" name="unit_of_measurement" value="{{$data->unit_of_measurement}}">
							
						</div>

						<div class="form-group  col-md-6 modal-input">
		
							<label class="control-label" for="name">Supplier</label>
							<select class="form-control select2" name="supplier">
								<option selected>{{$data->supplier}}</option>
								@foreach($suppliers as $supplier)
								<option>{{$supplier->supplier_name}}</option>
								@endforeach
							</select>
							
						</div>


					</div>

					<div class="modal-card em6">


						<div class="form-group  col-md-12 ">
		
							<label class="control-label" for="name">Current Price</label>
							<input type="text" class="form-control" name="current_price" placeholder="Current Price" value="{{$data->current_price}}">
							
						</div>
						
					</div>

					<input type="hidden" name="price_history" value="{{$data->current_price}}">

					<div class="modal-card em6" style="margin-top: 2em; height: 50px; overflow-y: auto;">


						<div class="form-group  col-md-12 ">
		
							<label class="control-label" for="name">Price History</label>

							@php
								$price_history = json_decode($data->price_history);
							@endphp
							
							@if($price_history != null)
								@foreach($price_history as $item)
									<p>{{$item}}</p>
								@endforeach
							@endif
							
						</div>
						
					</div>

				</form>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-red" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-blue" onclick="submit_form{{$data->id}}()">Done</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	
	function submit_form{{$data->id}}(){
		$('#form_edit{{$data->id}}').submit();
	}

</script>