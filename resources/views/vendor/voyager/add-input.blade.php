@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if($edit)
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            <div class="form-group col-md-6">
                                <label>Menu Name</label>
                                <input type="text" class="form-control" name="menu_name">
                            </div>

                           
                            <div id="base"> {{-- BASE=================== --}}

                                <div class="form-group col-md-6">
                                    <label>Ingredient</label>

                                    <select class="form-control" name="main_dish[]">
                                        
                                         @foreach(App\Dish::where('category', '=', 'main')->get() as $item)
                                            <option value="{{$item->dish_name}}">{{$item->dish_name}}</option>
                                        @endforeach

                                    </select>


                                </div>

                                <div class="form-group col-md-6">
                                    <label>Ingredient</label>

                                    <select class="form-control" name="main_dish[]">
                                        
                                         @foreach(App\Dish::where('category', '=', 'sidedish')->get() as $item)
                                            <option value="{{$item->dish_name}}">{{$item->dish_name}}</option>
                                        @endforeach

                                    </select>


                                </div>

                            </div> {{-- end base --}}



                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            @section('submit-buttons')
                                <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                            @stop
                            @yield('submit-buttons')
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>


@stop

@section('javascript')

@stop
