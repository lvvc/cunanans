@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if($edit)
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="form-group col-md-12">
                                <label>Recipe Name</label>
                                <input type="text" class="form-control" name="recipe_name">
                            </div>

                            <div id="base"> {{-- BASE=================== --}}

                                <div class="form-group col-md-6">
                                    <label>Ingredient</label>

                                    <select class="form-control" name="ingredient[]">
                                        
                                         @foreach($ingredients as $item)
                                            <option value="{{$item->id}}">{{$item->item_name}}</option>
                                        @endforeach

                                    </select>


                                </div>
                                <div class="form-group col-md-6">
                                    <label>Weight</label>
                                    <input type="text" class="form-control" name="weight[]">
                                </div>

                            </div> {{-- end base --}}

                            <div class="col-md-12">
                                <a href="#" id="add" class="btn-primary btn"><i class="voyager-plus"></i></a>
                            </div>




                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            @section('submit-buttons')
                                <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                            @stop
                            @yield('submit-buttons')
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>



<script type="text/javascript">
$(document).ready(function(e){
    var html = `
    <div id="container">
        

        <div class="form-group col-md-6">
            <select class="form-control" name="ingredient[]">
                                        
            @foreach($ingredients as $item)
                <option value="{{$item->item_name}}">{{$item->item_name}}</option>
            @endforeach

            </select>
        </div>

        <div class="form-group col-md-5">
            <input type="text" class="form-control" name="weight[]">
        </div>

        <div class="form-group" style="text-align:center;">

            <a href="#" id="remove" style="margin-top:0px" class="btn-danger btn"><i class="voyager-trash"></i></a>
            
        </div>
    
    </div>
    `;
    let count = 0;
    $('#add').click(function(e){
        if(count > 20){
            alert('limit reached');
            exit();
        };
        $('#base').append(html);
        count++;
    });
        
    $('.panel-body').on('click', '#remove', function(e){
        $(this).parent('div').parent('div').remove();
        count--;
    });
    
});
</script>





@stop